import javax.swing.*;
import java.awt.*;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.util.ArrayList;
import java.util.concurrent.ThreadLocalRandom;

/** El panell s'encarrega de crear els virus, afegir-los a una llista i pintar-los
 * al mètode paint()
 * @author Manel
 * @version 07/11/2021
 */
class PanelVirus extends JPanel implements MouseListener{
    private ArrayList<Virus> virus = new ArrayList<>();

    /**
     * Afegeix un mouseListener al constructor del PanelVirus per control·lar el input del ratolí
     */
    public PanelVirus() {
        addMouseListener(this);
    }

    /**
     * Crea un nombre aleatori de virus entre 5 i 10 i els afegeix a la llista virus
     */
    public void addVirus() {
        for(int i = 0; i < randomRange(5, 10); i++)
        {
            Virus v = new Virus(this);
            virus.add(v);
            Thread t = new Thread(v);
            t.start();
        }
    }

    /** Mètode que no es pot canviar de nom. Quan es vol pintar un objecte gràfic sobre un Component
     * de Swing de Java.
     * @param g de tipus Graphics
     */
    public void paint(Graphics g) {
        super.paint(g);
        for (Virus v : virus)
        {
            v.paintVirus(g);
        }
        Toolkit.getDefaultToolkit().sync();
    }

    /**
     * Retorna un nombre aleatori entre un rang mínim i màxim inclusius
     * @param min rang mínim
     * @param max rang màxim
     * @return el nombre aleatori
     */
    public int randomRange(int min, int max)
    {
        return ThreadLocalRandom.current().nextInt(min, max + 1);
    }

    @Override
    public void mouseClicked(MouseEvent e) {
    }

    /**
     * Lògica a executar en clicar el ratolí en la pantalla.
     * Per cada virus s' extrau un rang inferior i superior d' X i Y.
     * Si el click del mouse ha estat dintre d' algun d' aquests rangs matem al/s virus
     * @param e paràmetre de l' event del MouseListener, conté les coordenades X i Y d' on estava el ratolí quan ha sigut clicat
     */
    @Override
    public void mousePressed(MouseEvent e) {
        int x = e.getX();
        int y = e.getY();
        for (Virus v : virus)
        {
            int infXRange = (int)v.getX() - v.getRadi();
            int supXRange = (int)v.getX() + v.getRadi();
            int infYRange = (int)v.getY() - v.getRadi();
            int supYRange = (int)v.getY() + v.getRadi();
            if(x >= infXRange && x <= supXRange && y >= infYRange && y <= supYRange)
            {
                v.killVirus();
            }
        }

    }

    @Override
    public void mouseReleased(MouseEvent e) {

    }

    @Override
    public void mouseEntered(MouseEvent e) {

    }

    @Override
    public void mouseExited(MouseEvent e) {

    }
}