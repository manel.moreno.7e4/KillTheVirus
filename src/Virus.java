import javax.swing.*;
import java.awt.*;
import java.awt.geom.Rectangle2D;
import java.net.URL;
import java.util.Random;
/**
 * Classe bàsica Virus. Versió implementant Runnable
 * Un virus que es mou i rebota dintre d'uns límits
 * Els límits venen donats per un panell (JPanel) contenidor
 *
 * @author Manel
 * @version 07/11/2021
 *
 */
public class Virus implements Runnable{

    private double x = 6;
    private double y = 6;
    private Image virusImage;
    private double dx = 1;
    private double dy = 1;
    private int radi = 40;
    private final PanelVirus commonPanel;
    private boolean isAlive = true;

    public int getRadi() {
        return radi;
    }

    public double getX() {
        return x;
    }

    public double getY() {
        return y;
    }

    /**
     * Constructor del virus. Randomitza els valors inicials de posició i velocitat i assigna la imatge del virus viu a la variable virusImage
     * @param commonPanel assigna el panell JPanel comú a tots els virus en crear-los
     */
    public Virus(PanelVirus commonPanel){
        this.commonPanel = commonPanel;
        randomizeInitValues();
        try{
            URL url = getClass().getClassLoader().getResource("virus.Jpg");
            virusImage=new ImageIcon(url).getImage();
        } catch(Exception e){e.printStackTrace();}
    }

    /**
     * Mou els virus per la pantalla.
     */
    public void mooveVirus() {
        Rectangle2D limits = commonPanel.getBounds();
        double width = limits.getWidth();
        double height = limits.getHeight();
        x += dx;
        y += dy;
        if (x + radi / 2 > width || x + radi / 2 < 0) dx = -dx;
        if (y + radi / 2 > height || y + radi / 2 < 0) dy = -dy;
    }

    /**
     * Es crida des del PanelVirus cada cop que es pinta un virus
     * @param g de tipus Graphics, passat per la funció paint() de Swing al PanelVirus des de JPanel
     */
    public void paintVirus(Graphics g)
    {
        Graphics2D g2 = (Graphics2D) g;
        g2.drawImage(virusImage, (int)x, (int)y, radi, radi, null);
    }

    /**
     * En matar un virus isAlive passa a false i es substitueix la virusImage
     */
    public void killVirus()
    {
        isAlive = false;
        try{
            URL url = getClass().getClassLoader().getResource("deadVirus.Jpg");
            virusImage=new ImageIcon(url).getImage();
        } catch(Exception e){e.printStackTrace();}
    }

    /**
     * Funció run que executará cada Thread de virus, mou els virus per la pantalla sempre que estiguin vius
     */
    @Override
    public void run() {
        while (isAlive)
        {
            mooveVirus();
            try {
                Thread.sleep(4);
            } catch (Exception e) {
            }
            commonPanel.repaint();
        }
    }

    /**
     * Retorna un double random entre un rang minim i màxim
     * @param min rang mínim
     * @param max rang màxim
     * @return el nombre aleatori
     */
    private double randomRangeDouble(int min, int max)
    {
        Random r = new Random();
        return min + (max - min) * r.nextDouble();
    }

    /**
     * Randomitza els valors inicials de posició i velocitat dels virus
     */
    private void randomizeInitValues()
    {
        x = commonPanel.randomRange(1, commonPanel.getWidth());
        y = commonPanel.randomRange(1, commonPanel.getHeight());
        dx = randomRangeDouble(0, 2);
        dy = commonPanel.randomRange(0, 2);
    }
}